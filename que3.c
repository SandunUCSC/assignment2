#include <stdio.h>
int main()
{
    int x, y, z;
    printf("Enter two integers which you want to swap\n");
    scanf("%d%d", &x, &y);
    printf("Integers before swapping\n 1st Integer = %d\n 2nd Integer = %d\n", x, y);
    z=x;
    x=y;
    y=z;
    printf("Integers after swapping\n 1st Integer =%d\n 2nd Integer = %d\n", x, y);
    return 0;
}
